# Git History Test

```
$ mkdir system_one
$ echo -e "Config1\n-------\nDirective1\nDirective2\nDirective3" > system_one/config.txt
$ git add system_one/
$ git commit -m "Added configuration file one"
$ mkdir system_two
$ echo -e "Config2\n-------\nDirective1\nDirective2\nDirective3\nDirective4\nDirective5" > system_two/config.txt
$ git add system_two/
$ git commit -m "Added configuration file two"
$ git log --follow --name-status system_two/config.txt
$ mkdir system_three
$ echo -e "Config3\n-------\nDirectiveA\nDirectiveB\nDirective3\nDirective4\nDirective5" > system_three/config.txt
$ git add system_three
$ git commit -m "Added configuration file three"
```

Then:

```
$ git log --follow --name-status system_three/config.txt
commit 17af4be46e785b8df32de0a40a62cdf0e032c5fa (HEAD -> master, origin/master)
Author: Jason Young <jyoung@gitlab.com>
Date:   Mon Apr 20 16:38:34 2020 -0400

    Added configuration file three

C057    system_two/config.txt   system_three/config.txt

commit 294ce9dd1ef6e9dfb776585b3b69874eb4b2897a
Author: Jason Young <jyoung@gitlab.com>
Date:   Mon Apr 20 16:35:35 2020 -0400

    Added configuration file two

C057    system_one/config.txt   system_two/config.txt

commit 8e321156c4b30268aac9d8ff456b3983e18e9e56
Author: Jason Young <jyoung@gitlab.com>
Date:   Mon Apr 20 16:34:29 2020 -0400

    Added configuration file one

A       system_one/config.txt
```